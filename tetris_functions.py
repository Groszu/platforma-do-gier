import pygame
import random

from tetris_variable import *


class Block(object):

    def __init__(self, x, y, shape):
        self.x = x
        self.y = y
        self.shape = shape
        self.color = Colors_of_shapes[shapes.index(shape)]
        self.rotation = 0


def create_play_field(locked):
    play_field = [[(0, 0, 0) for x in range(10)] for x in range(20)]

    for i in range(len(play_field)):
        for j in range(len(play_field[i])):
            if (j, i) in locked:
                k = locked[(j, i)]
                play_field[i][j] = k
    return play_field


def convert_shape_format(shape):
    positions = []
    block_format = shape.shape[shape.rotation % len(shape.shape)]

    for i, line in enumerate(block_format):
        for j, column in enumerate(list(line)):
            if column == '0':
                positions.append((shape.x + j, shape.y + i))

    for i, pos in enumerate(positions):
        positions[i] = (pos[0] - 2, pos[1] - 4)

    return positions


def valid_space(shape, grid):
    valid_positions = [[(j, i) for j in range(10) if grid[i][j] == (0, 0, 0)] for i in range(20)]
    valid_positions = [j for sub in valid_positions for j in sub]
    formatted = convert_shape_format(shape)

    for pos in formatted:
        if pos not in valid_positions:
            if pos[1] > -1:
                return False

    return True


def lost(positions):
    for pos in positions:
        x, y = pos
        if y < 1:
            return True
    return False


def create_shape():
    return Block(5, 0, random.choice(shapes))


def clear_rows(grid, locked, score):

    del_rows = 0
    for i in range(len(grid)):
        if (0, 0, 0) not in grid[i]:
            del_rows += 1
            score += 100
            ind = i
            for j in range(len(grid[i])):
                try:
                    del locked[(j, i)]
                except:
                    continue

    if del_rows > 0:
        for key in sorted(list(locked), key=lambda x: x[1])[::-1]:
            x, y = key
            if y < ind:
                new = (x, y + del_rows)
                locked[new] = locked.pop(key)
    return score


def draw_window(screen, grid):

    for i in range(len(grid)):
        for j in range(len(grid[i])):
            pygame.draw.rect(screen, grid[i][j], (top_left_x + j * 40, 0 + i * 40, 40, 40), 0)

    pygame.draw.rect(screen, (128, 128, 128), (top_left_x, 0, play_width, play_height), 4)


def draw_score(text, size, color, screen):

    font = pygame.font.SysFont('times', size, bold=True)
    label = font.render("score: " + text, 1, color)

    screen.blit(label, (screen_width - 150 - (label.get_width() / 2), top_left_y + play_height - 700 - label.get_height() / 2))


def draw_score_lost(text, size, color, surface):
    font = pygame.font.SysFont('times', size, bold=True)
    label = font.render("Your score is: " + text, 1, color)

    surface.blit(label, (top_left_x + play_width / 2 - (label.get_width() / 2), top_left_y + play_height / 2 - label.get_height() / 2))



