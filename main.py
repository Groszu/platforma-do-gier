import pygame
import random
from tetris_functions import *
from tetris_variable import *
from snake_classes import *

pygame.font.init()

def snake_game():
    game = Game()
    snake = Snake()
    food = Food()


    window = pygame.display.set_mode(game.resolution)
    fps = pygame.time.Clock()

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                menu()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_d:
                    snake.changeDirTo('RIGHT')
                if event.key == pygame.K_w:
                    snake.changeDirTo('UP')
                if event.key == pygame.K_s:
                    snake.changeDirTo('DOWN')
                if event.key == pygame.K_a:
                    snake.changeDirTo('LEFT')
                if event.key == pygame.K_ESCAPE:
                    game.endGame = True

        foodPos = food.spawnFood()
        if snake.move(foodPos) == 1:
            game.score += 1
            food.setFoodOnScreen(False)
        window.fill(pygame.Color(189, 252, 147))

        for pos in snake.getBody():
            pygame.draw.rect(window, game.snakebodycolor, pygame.Rect(pos[0], pos[1], 20, 20))
        pygame.draw.rect(window, pygame.Color(225, 0, 0), pygame.Rect(foodPos[0], foodPos[1], 20, 20))

        if snake.checkCollision() == 1 or game.endGame:

            current_best = open('score_snake.txt').read()
            if int(current_best) < game.score:
                file = open('score_snake.txt', 'w')
                file.write(str(game.score))
                file.close()

            while True:
                window.fill((0, 0, 0))
                draw_score_snake(str(game.score), 30, (255, 255, 255), window)
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        menu()
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_ESCAPE:
                            menu()
                        if event.key == pygame.K_SPACE:
                            snake_game()

        pygame.display.update()



        pygame.display.set_caption("SNAKE by Andrzej Osowski | Score : " + str(game.score))
        pygame.display.flip()
        fps.tick(game.setLevel())

def tetris():

    pygame.display.set_caption('Tetris')
    score = 0
    locked_positions = {}
    grid = create_play_field(locked_positions)
    end_game = False
    change_piece = False
    run = True
    current_block = create_shape()
    next_block = create_shape()
    clock = pygame.time.Clock()
    fall_time = 0

    while run:

        block_speed = 0.28

        grid = create_play_field(locked_positions)
        fall_time += clock.get_rawtime()
        clock.tick()

        if fall_time / 1000 >= block_speed:
            fall_time = 0
            current_block.y += 1
            if not (valid_space(current_block, grid)) and current_block.y > 0:
                current_block.y -= 1
                change_piece = True

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
                pygame.display.quit()
                quit()

            if event.type == pygame.KEYDOWN:

                if event.key == pygame.K_a:
                    current_block.x -= 1
                    if not valid_space(current_block, grid):
                        current_block.x += 1

                if event.key == pygame.K_d:
                    current_block.x += 1
                    if not valid_space(current_block, grid):
                        current_block.x -= 1

                if event.key == pygame.K_w:
                    current_block.rotation = current_block.rotation + 1 % len(current_block.shape)
                    if not valid_space(current_block, grid):
                        current_block.rotation = current_block.rotation - 1 % len(current_block.shape)

                if event.key == pygame.K_s:
                    current_block.y += 1
                    if not valid_space(current_block, grid):
                        current_block.y -= 1

                if event.key == pygame.K_ESCAPE:
                    end_game = True

        shape_pos = convert_shape_format(current_block)

        for i in range(len(shape_pos)):
            x, y = shape_pos[i]
            if y > -1:
                grid[y][x] = current_block.color

        if change_piece:
            for pos in shape_pos:
                p = (pos[0], pos[1])
                locked_positions[p] = current_block.color
            current_block = next_block
            next_block = create_shape()
            change_piece = False

            score = clear_rows(grid, locked_positions, score)

        window.fill((0, 0, 0))
        draw_window(window, grid)
        draw_score(str(score), 60, (255, 255, 255), window)
        pygame.display.update()

        if lost(locked_positions) or end_game:

            current_best = open('score_tetris.txt').read()
            if int(current_best) < score:
                file = open('score_tetris.txt', 'w')
                file.write(str(score))
                file.close()

            while run:
                window.fill((0, 0, 0))
                draw_score_lost(str(score), 60, (255, 255, 255), window)
                pygame.display.update()
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        menu()
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_ESCAPE:
                            menu()
                        if event.key == pygame.K_SPACE:
                            tetris()

    pygame.display.update()


def score():

    window.fill((0, 0, 0))
    pygame.display.set_caption('Wyniki')
    font = pygame.font.SysFont('times', 60, bold=True)
    score_tetris = open('score_tetris.txt').read()
    score_snake = open('score_snake.txt').read()
    # score_pong = open('score_pong.txt').read()
    label_m = font.render("Wyniki", 1, (66, 175, 226))
    label_t = font.render("Tetris: " + score_tetris, 1, (255, 255, 255))
    label_s = font.render("Snake: " + score_snake, 1, (255, 255, 255))
    # label_p = font.render("Pong: " + score_pong, 1, (255, 255, 255))
    label_exit = font.render("Powrót(ESC)", 1, (255, 65, 0))

    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.display.quit()
                quit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    menu()

        window.blit(label_m, (400, 80))
        window.blit(label_t, (400, 220))
        window.blit(label_s, (400, 320))
        # window.blit(label_p, (400, 420))
        window.blit(label_exit, (400, 650))
        pygame.display.update()


def menu():
    window = pygame.display.set_mode((screen_width, screen_height))
    window.fill((0, 0, 0))
    pygame.display.set_caption('Menu')
    font = pygame.font.SysFont('times', 65, bold=True)
    label_m = font.render("Menu", 1, (66, 175, 226))
    label_t = font.render("1. Tetris", 1, (255, 255, 255))
    label_s = font.render("2. Snake", 1, (255, 255, 255))
    label_p = font.render("3. Pong", 1, (255, 255, 255))
    label_score = font.render("4. Wyniki", 1, (255, 255, 255))
    label_exit = font.render("Wyjscie(ESC)", 1, (255, 65, 0))

    while True:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.display.quit()
                quit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    tetris()
                if event.key == pygame.K_2:
                    snake_game()
                if event.key == pygame.K_3:
                    # pong()
                    pass
                if event.key == pygame.K_4:
                    score()
                if event.key == pygame.K_ESCAPE:
                    pygame.display.quit()
                    quit()

        window.blit(label_m, (400, 80))
        window.blit(label_t, (400, 220))
        window.blit(label_s, (400, 320))
        window.blit(label_p, (400, 420))
        window.blit(label_score, (400, 520))
        window.blit(label_exit, (400, 650))
        pygame.display.update()


pygame.display.set_caption('Menu')

menu()
