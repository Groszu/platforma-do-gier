import sys
import random


class Snake:
    def __init__(self):
        self.position = [180, 180]
        self.body = [[180, 180], [160, 180], [140, 180]]
        self.direction = "RIGHT"

    def changeDirTo(self, dir):
        if dir == "RIGHT" and not self.direction == "LEFT":
            self.direction = "RIGHT"
        if dir == "LEFT" and not self.direction == "RIGHT":
            self.direction = "LEFT"
        if dir == "UP" and not self.direction == "DOWN":
            self.direction = "UP"
        if dir == "DOWN" and not self.direction == "UP":
            self.direction = "DOWN"

    def move(self, foodPos):
        if self.direction == "RIGHT":
            self.position[0] += 20
        if self.direction == "LEFT":
            self.position[0] -= 20
        if self.direction == "UP":
            self.position[1] -= 20
        if self.direction == "DOWN":
            self.position[1] += 20
        self.body.insert(0, list(self.position))
        if self.position == foodPos:
            return 1
        else:
            self.body.pop()
            return 0

    def checkCollision(self):
        if self.position[0] > 380 or self.position[0] < 0:
            return 1
        elif self.position[1] > 380 or self.position[1] < 0:
            return 1
        for bodyPart in self.body[1:]:
            if self.position == bodyPart:
                return 1
        return 0

    def getHeadPos(self):
        return self.position

    def getBody(self):
        return self.body


class Food:
    def __init__(self):
        self.position = [random.randrange(1, 20) * 20, random.randrange(1, 20) * 20]
        self.isFoodOnScreen = True

    def spawnFood(self):
        if self.isFoodOnScreen == False:
            self.position = [random.randrange(1, 20) * 20, random.randrange(1, 20) * 20]
            if self.position == snake.body[:]:
                self.position = [random.randrange(1, 20) * 20, random.randrange(1, 20) * 20]

            self.isFoodOnScreen = True
        return self.position

    def setFoodOnScreen(self, b):
        self.isFoodOnScreen = b


class Game():
    def __init__(self):
        self.resolution = (400, 400)
        self.score = 0
        self.snakebodycolor = (252, 255, 30)

    def gameOver(self):
        sys.exit()


    def setLevel(self):
        if self.score >= 0 and self.score <= 10:
            self.snakebodycolor = (252, 255, 30)
            return 8

        elif self.score >= 11 and self.score <= 20:
            self.snakebodycolor = (252, 166, 74)
            return 10
        elif self.score >= 20 and self.score <= 30:
            self.snakebodycolor = (255, 105, 74)
            return 12
        else:
            self.snakebodycolor = (255, 40, 40)
            return 14






